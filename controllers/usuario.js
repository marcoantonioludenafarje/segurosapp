'use strict'
var bcrypt=require('bcrypt-nodejs');
var User=require('../models/usuario');
function pruebas(req,res){
    res.status(200).send({message:'Probando una acccion del controlador'});
}


function saveUser(req,res){
    var user=new User();
    var params=req.body;
    user.dni=params.dni;
    user.correo=params.correo;
    user.telefono=params.telefono;
    user.direccion=params.direccion;
    user.nombre=params.nombre;
    user.apellido=params.apellido;
    user.role='ROLE_USER';
    user.image='null';
    console.log("Me llamaron saveUSer");
if(params.password){
    //encriptar contraseña y guardar datos
    bcrypt.hash(params.password,null,null,function(err,hash){
        user.password=hash;
        if(user.nombre !=null && user.apellido !=null && user.correo !=null){
            //guarde el usuario en la base de datos
            user.save((err,userStored)=>{
                if(err){
                    res.status(500).send({message:'Error alguardar el usuario'});
                }else{
                    if(!userStored){
                        res.status(404).send({message:'No se ha registrado el usuario'});
                    }else{
                        res.status(200).send({user:userStored});
                    }
                }
            });

        }else{
            res.status(200).send({message:'Rellena todos los campos'});
        }

    });

}else{
    res.status(200).send({message:'Introduce la contraseña'});
}


}
module.exports={
    pruebas,
    saveUser
}