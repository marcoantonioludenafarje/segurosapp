'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var MisCosasSchema=Schema({
    fecha_inscripcion:String,
    foto:String,
    nro_veces_asegurado:Number,
    fecha_compra:String,
    foto_boucher:String,
    usuario:{type:Schema.ObjectId, ref:'Usuario'},
    cosa_estandar:{type:Schema.ObjectId, ref:'CosasEstandar'}
});
module.exports=mongoose.model('MisCosas',MisCosasSchema);