'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var CategoriaSchema=Schema({
    nombre:String,
    descripcion:String,
    nro_prod:Number
});
module.exports=mongoose.model('Categoria',CategoriaSchema);