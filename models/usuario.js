'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var UserSchema=Schema({
    dni:String,
    correo:String,
    telefono:String,
    direccion:String,
    nombre:String,
    apellido:String,
    password:String,
    role:String,
    image:String
});
module.exports=mongoose.model('Usuario',UserSchema);