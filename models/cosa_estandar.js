'use strict'

var mongoose=require('mongoose');
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;
var CosasEstandarSchema=Schema({
    marca:String,
    modelo:String,
    descripcion:String,
    precio:Number,
    foto:String,
    categoria:{type:Schema.ObjectId, ref:'Categoria'}

});
module.exports=mongoose.model('CosasEstandar',CosasEstandarSchema);